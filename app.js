var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';
var app = express();
//global.__base = __dirname + '/';

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));

app.get('*', function(req, res){
    res.render('landing');
});

app.set('port', process.env.PORT || 3000);

app.listen(app.get('port'), function(){
    console.log('Express is here on port '+ app.get('port') +'!');
});