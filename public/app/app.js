angular.module('landing', [
    'ngAnimate',
    'ngResource',
    'ngRoute',
    //'landing.services',
    'landing.controllers',
    'landing.directives'
])

    .config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
        $locationProvider.html5Mode({
            enabled:  true,
            requireBase: false
        });
        $routeProvider
            .when('/', {
                templateUrl: '/templates/partials/home.html',
                controller: 'LandingController'
            });
    }]);
