/*global $:false */
angular.module('landing.directives', [])

    .directive('cells', ['$interval', function($interval){
        return {
            restrict: 'A',
            link: function(scope, ele, attrs){
                var offset = ele.offset();
                var top = offset.top;
                var height = ele.height();
                var gridH = $('#grid').height();
                var bottom;
                var moveToTop = $interval(function(){
                    top -= 1;
                    bottom = top + height;
                    if(bottom <= 0){
                        top = gridH - height;
                    }
                    ele.offset({top: top, left: 0});
                }, 50);
            }
        };
    }])

    .directive('category', function(){
        return {
            restrict: 'A',
            link: function(scope, ele, attrs){
                //$(ele).css({"-webkit-transition-delay" : ".3", "transition-delay" : ".3"});
                var delay = Math.floor((Math.random() * 3) + 1)/10;
                delay += "s";
                ele.css('-webkit-transition-delay', delay);
                ele.css('transition-delay', delay);
                console.log('yay!');
            }
        };
    })

    .directive('adaptiveInput', function(){
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function(scope, ele, attrs, ngModel){
                var initialFontSize = ele.css('font-size');
                var fontSizeNum = initialFontSize.substring(0, initialFontSize.length - 2);
                var fsize = fontSizeNum;
                var fsizeCof = fontSizeNum / 3;
                var breakPoints = [];

                scope.validateEmail = function(email) { 
                    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    return re.test(email);
                };
                
                scope.$watch(function(){
                        return ngModel.$modelValue;
                    }, function(n){
                    if(!n) return;
                    if(ele.prop('scrollWidth') >= window.innerWidth){
                        breakPoints.push(n.length);
                        fsize = parseFloat(fsize) - parseFloat(fsizeCof);
                        ele.css({'font-size' : fsize});
                    }
                    else if (breakPoints.length > 0){
                        if(breakPoints[breakPoints.length - 1] > n.length){
                            fsize = parseFloat(fsize) + parseFloat(fsizeCof);
                            ele.css({'font-size' : fsize});
                            breakPoints.pop();  
                        }
                    }
                    var valid = scope.validateEmail(n);
                    if(!valid) { ele.addClass('ng-invalid'); }
                    else { ele.removeClass('ng-invalid'); }
                    scope.subs.err = 0; // clear errors if field is changing
                });
            }
        }
    });
