angular.module('landing.controllers', [])
    .controller('LandingController', ['$scope', function($scope){
    	$scope.page = {
    		categories: false,
    		subscribe: false
    	};
    	
    	$scope.subs = $scope.subs || {};

    	$scope.toggleCategories = function(){
    		$scope.page.categories = $scope.page.categories ? false : true;
    	};

    	$scope.toggleSubs = function(){
    		$scope.page.subscribe = $scope.page.subscribe ? false : true;
    	};
    }]);
