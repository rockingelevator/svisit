var gulp = require('gulp');
var jade = require('gulp-jade');
var uglify = require('gulp-uglify');
var ngTemplates = require('gulp-ng-templates');
var watch = require('gulp-watch');
var stylus = require('gulp-stylus');
var connect = require('gulp-connect');
var concat = require('gulp-concat');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');

//compile templates .jade to html
gulp.task('templates', function(){
    gulp.src('./views/*.jade')
        .pipe(jade({
            pretty: true
        }))
        .pipe(gulp.dest('./public/templates/'))
        .pipe(gulp.dest('./public/dist/templates/'))
});

//compile partials .jade to html
gulp.task('partials', function(){
   gulp.src('./views/partials/*.jade')
       .pipe(jade({
           pretty: true
       }))
       .pipe(gulp.dest('./public/templates/partials/'))
});

gulp.task('ngtemplates', function(){
    return gulp.src('./public/templates/partials/*.html')
        .pipe(ngTemplates({
            filename: 'partials.js',
            module: 'app',
            path: function (path, base) {
                //return path.replace(base, '').replace('/partials', '');
                return path.replace(base, '/templates/partials/');
            }
        }))
        .pipe(uglify())
        //.pipe(rename({
        //    suffix: '.min'
        //}))
        .pipe(gulp.dest('./public/dist/templates'));
});

gulp.task('stylus', function(){
    gulp.src('./styl/*.styl')
        .pipe(stylus())
        .pipe(gulp.dest('./public/css/'))
        .pipe(concat('bundle.css'))
        .pipe(gulp.dest('./public/dist/css/'))
        .pipe(minifyCss({keepBreaks: true}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./public/dist/css/'));
});


gulp.task('connect', function(){
    connect.server();
});

gulp.task('default', ['templates', 'partials', 'ngtemplates', 'stylus']);

